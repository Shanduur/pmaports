# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-workspace
pkgver=5.20.90
pkgrel=1
pkgdesc="KDE Plasma Workspace"
# armhf blocked by kirigami2
# s390x blocked by kactivitymanagerd
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-or-later AND MIT AND LGPL-2.1-only AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
depends="
	kactivitymanagerd
	kded
	kinit
	kio-extras
	kirigami2
	kquickcharts
	kwin
	milou
	plasma-integration
	qt5-qtquickcontrols
	qt5-qttools
	qtchooser
	tzdata
	"
depends_dev="
	appstream-dev
	baloo-dev
	gpsd-dev
	iso-codes-dev
	kactivities-stats-dev
	kcmutils-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kded-dev
	kdelibs4support-dev
	kdesu-dev
	kglobalaccel-dev
	kholidays-dev
	ki18n-dev
	kidletime-dev
	kitemmodels-dev
	kjsembed-dev
	knewstuff-dev
	knotifyconfig-dev
	kpackage-dev
	kpeople-dev
	krunner-dev
	kscreenlocker-dev
	ktexteditor-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kwallet-dev
	kwayland-dev
	kwin-dev
	libkscreen-dev
	libksysguard-dev
	networkmanager-qt-dev
	phonon-dev
	plasma-framework-dev
	prison-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	libxtst-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	dont-crash-if-panelview-not-ready.patch
	"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"
replaces="plasma-desktop<5.20"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		for i in $(find -name CMakeLists.txt); do
			echo "Disabling ecm_find_qmlmodule in: $i"
			sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" "$i"
		done
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# nightcolortest requires running dbus
	# testdesktop, lookandfeel-kcmTest, test_kio_fonts, servicerunnertest and systemtraymodeltest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(nightcolortest|testdesktop|lookandfeel-kcmTest|test_kio_fonts|servicerunnertest|systemtraymodeltest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="06c75637600053a8329791c58d2500f9f29f539e9b54a430f3ac7d81d8515a5ef412f972a0b564b46081da558b8facbce4d3f713ebd658e0e66350b409425b87  plasma-workspace-5.20.90.tar.xz
96dd28bd628bea742732569a6ebdee8c18d2bded7b52ec30f859eb133e3dd6c9a5a33aced33e216f31999e536e20de6e33ea542bb1f1613b919043f0ac3aba05  dont-crash-if-panelview-not-ready.patch"
