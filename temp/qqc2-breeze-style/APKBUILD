# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qqc2-breeze-style
pkgver=5.20.90
pkgrel=0
pkgdesc="Breeze inspired QQC2 style"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="LicenseRef-KDE-Accepted-LGPL AND LicenseRef-KFQF-Accepted-GPL"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kguiaddons-dev
	kiconthemes-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/qqc2-breeze-style-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="fa27d1f7eab7a784678caeff32aa3f2bf9cfd6ab4132a564c056b7f5ba56460b48dd05490dee60fe3c5d24b4ec5cfcc7da20bb2404d8fbbcc9ae399c629677b5  qqc2-breeze-style-5.20.90.tar.xz"
