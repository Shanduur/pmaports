# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeplasma-addons
pkgver=5.20.90
pkgrel=0
pkgdesc="All kind of addons to improve your Plasma experience"
# mips, ppc64le and s390x blocked by qt5-qtwebengine
# armhf blocked by qt5-qtdeclarative
arch="all !ppc64le !s390x !armhf !mips !mips64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.0-only AND GPL-2.0-or-later"
depends="purpose"
depends_dev="
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdeclarative-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	krunner-dev
	kservice-dev
	kunitconversion-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	sonnet-dev
	"

makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# converterrunnertest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "converterrunnertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="5c8b3f9828ffde42d62b75f9a46a1597765a922b5a93c9ff10444a6284fbae430d58ac107baec530e4e1858915daa33f960a892dc1ce5ea3d47b108bf3d91a44  kdeplasma-addons-5.20.90.tar.xz"
